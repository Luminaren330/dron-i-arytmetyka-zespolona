#ifndef MACIERZ2X2_HH
#define MACIERZ2X2_HH


#include <iostream>
#include <vector>
#include "../inc2/Wektor.hh"

/*!
* \brief Modeluje pojęcie Macierz rotacji
*
* Klasa tworzy macierz rotacji o danym rozmiarze.
* Posiada przeciążenia operatorów pozwalające wykonywać różne operacje matematyczne.
* Zawiera również konstruktor.
* Jej atrybutem jest pole zawierające wiersze wektorów.
* */
template <int Rozmiar>
class MacierzRot {
  private:
  /*! 
  *\brief Wiersze macierzy
  *
  * Vector o danych klasy Wektor, zawiera ilość wierszy macierzy */
  std::vector<Wektor<Rozmiar>> wiersze;
  public:
   MacierzRot<Rozmiar> operator * (const MacierzRot<Rozmiar> & Arg2) const;
   Wektor<Rozmiar>  operator * (const Wektor<Rozmiar> & Punkt) const;
   MacierzRot(double kat_stopnie, char os);
   MacierzRot(); 
   const Wektor<Rozmiar> & operator[] (int ind) const;
};

template <int Rozmiar>
std::ostream& operator << (std::ostream &Strm, const MacierzRot<Rozmiar> &Mac);

#endif

