#ifndef WEKTOR2D_HH
#define WEKTOR2D_HH

#include <iostream>
#include <vector>

/*!
* \brief Modeluje pojęcie Wektor
*
* Klasa tworzy wektor o danym rozmiarze.
* Posiada przeciążenia operatorów pozwalające wykonywać różne operacje matematyczne.
* Zawiera również konstruktory.
* Jej atrybutami jest pole zawierające współrzędne wektor,
* jak i wartości ile jest w danym momencie i ile stworzono wektorów.
* */
template <int Rozmiar>
class Wektor {
  /*! 
  * \brief Współrzędne wierzchołka
  *
  * Vector double zawiera współrzędne wektora o danym rozmiarze.
  * */
  std::vector<double> wsp;
  /*! 
  * \brief Warość liczby wektorów w danej chwili
  *
  * Pole zawiera ilość wektorów w danej chwili 
  *  */
  inline static int ile_jest=0;
  /*! 
  * \brief Warość liczby wektorów stworzonych
  *
  * Pole zawiera ilość wektorów wszystkich stworzonych. 
  *  */
  inline static int ile_stworzono=0;
  public:
  /*! 
  * \brief Operator przeciążenia + 
  * 
  * Dodaje współrzędne wektora
  * */
   Wektor<Rozmiar>  operator +(const Wektor<Rozmiar>  &Arg2) const;
   Wektor<Rozmiar>   operator -(const Wektor<Rozmiar>  &Arg2) const;
   Wektor<Rozmiar>   operator *(double &Arg2) const;
   double  operator *(const Wektor<Rozmiar>  &Arg2) const; 
    /*! 
  * \brief Konstruktor
  * 
  * Konstruktor domyślny przypisujący 0 do współrzędnych.
  * Przy tworzeniu wektora rośnie wartość stworzonych i w danej
  * chwili posiadania wektorów
  * */
   Wektor<Rozmiar>() {
    for (int i=0; i<Rozmiar; i++) {
        wsp.push_back(0);
    }
    ile_jest++; 
    ile_stworzono++;
    };
    /*! 
  * \brief Konstruktor
  * 
  * Konstruktor dający dostęp do współrzędnych.
  * \param[in] tmp- daje dostęp do współrzędnych
  * Przy tworzeniu wektora rośnie wartość stworzonych i w danej
  * chwili posiadania wektorów
  * */
   Wektor<Rozmiar>(std::initializer_list<double> tmp): wsp(tmp) {ile_jest++; ile_stworzono++;}; 
   /*! 
  * \brief Konstruktor
  * 
  * Konstruktor kopiujący współrzędne.
  * \param[in,out] W-wartość do której kopiujemy wartości współrzędnych wektora.
  * Przy tworzeniu wektora rośnie wartość stworzonych i w danej
  * chwili posiadania wektorów
  * */
   Wektor(const Wektor& W) {
     wsp=W.wsp;
     ile_jest++; 
     ile_stworzono++;
     };
     /*! 
  * \brief Dekonstruktor
  * 
  * Deonstruktor, który usuwa ilość wektorów
  * w danej chwili przy wykorzystaniu
  * */
   ~Wektor<Rozmiar>() {--ile_jest;};
   /*! 
  * \brief Metoda get_jest
  * 
  * \return zwraca ilość wektorów w danej chwili
  * */
   static int get_jest() {return ile_jest;};
   /*! 
  * \brief Metoda get_stworzono
  * 
  * \return zwraca ilość wszystkich stworzonych wektorów
  * */
   static int get_stworzono() {return ile_stworzono;};
   double dlugosc() const;
   const double & operator[] (int ind) const; 
   double & operator[] (int ind); 
     
};


template <int Rozmiar>
std::istream& operator >> (std::istream &Strm, Wektor<Rozmiar>  &Wek);
template <int Rozmiar>
std::ostream& operator << (std::ostream &Strm, const Wektor<Rozmiar>  &Wek);

#endif
