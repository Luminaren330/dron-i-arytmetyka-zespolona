#ifndef PROSTOKAT_HH
#define PROSTOKAT_HH

#include <iostream>
#include <vector>
#include "../inc2/Wektor.hh"
#include "../inc2/MacierzRot.hh"
#include "../inc2/Dr3D_gnuplot_api.hh"
#include <algorithm>
#include <ctime>
#include <math.h>
#include <memory>

class Scena;
/*!
* \brief Interfejs rysowania
*
* Klasa ta odpowiada za zjednolicenie rysowania elementów.
* Posiada metody wirtualne i 2 parametry o id i rysowniku.
* */
class Interfejs_rysowania {
  protected:
   /*! 
  * \brief id rysunku
  *
  * Pole to posiada id rysunku/przypisuje dane id do odpowiednich elementów.
  * */
  int id;
  /*! 
  * \brief rysownik
  *
  * Pole to posiada rysownik do GNUplota, co pozwala na rysowanie różnych kształtów.
  * */
  drawNS::Draw3DAPI *rysownik;
  public:
  /*! 
  * \brief Metoda virtualna do rysowania
  *
  * Ujednolicenie rysowania
  * */
  virtual void rysuj()=0;
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor, który pozwala na dostęp do rysownika
  * */
  Interfejs_rysowania(drawNS::Draw3DAPI *rysownik): rysownik(rysownik) {}; 
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor domyślny, który przypisuje do rysownika wartość NULL
  * */
  Interfejs_rysowania(): rysownik(NULL){}; 
  /*! 
  * \brief Metoda get_id
  *
  * Metoda, która zwraca id elementu
  * \return zwraca id elementu
  * */
  int get_id() {return id;}
};
/*!
* \brief Interfejs Drona
*
* Klasa ta odpowiada za zjednolicenie metod dronów.
* Posiada metody wirtualne i parametr id.
* */
class Interfejs_Drona {
  protected:
  /*! 
  * \brief id drona
  *
  * Pole to posiada id drona/przypisuje dane id do odpowiedniego drona.
  * */
  int id;
   public:
   /*! 
  * \brief Metoda virtualna do ruchu w przod 
  * */
   virtual void lec_w_przod(double ile)=0;
   /*! 
  * \brief Metoda virtualna do ruchu w góre
  * */
   virtual void lec_w_gore(double ile)=0;
   /*! 
  * \brief Metoda virtualna do obrotu
  * */
   virtual void obroc(double kat)=0;
   /*! 
  * \brief Metoda virtualna do animacji ruchu drona 
  * */
   virtual void animuj(double gora, double obrot, double przod, Scena Sc)=0;
   /*! 
  * \brief Metoda virtualna do zmazania drona z rysunku 
  * */
   virtual void zmaz_drona()=0;
   /*! 
  * \brief Metoda virtualna do obracania wirnikami drona 
  * */
   virtual void obroc_wirniki()=0;
   /*! 
  * \brief Metoda virtualna do narysowania linii ruchu drona
  * */
   virtual void rysuj_linie()=0;
   /*! 
  * \brief Metoda get_id
  *
  * Metoda, która zwraca id drona
  * \return zwraca id drona
  * */
   int get_id() {return id;} 
   /*! 
  * \brief Metoda virtualna do zdobycia środka drona 
  * */
   virtual Wektor<3> get_srodek2()=0;
   /*! 
  * \brief Metoda virtualna do zdobycia głębokosci drona
  * */
   virtual double get_glebokosc1()=0;
 };
/*!
* \brief Interfejs elemetów krajobrazu
*
* Klasa ta odpowiada za zjednolicenie metod elementów krajobrazu.
* Posiada metody wirtualne i parametr id.
* */
class Interfejs_el_krajobrazu {
  protected:
  /*! 
  * \brief id elementów krajobrazu
  *
  * Pole to posiada id elementów krajobrazu/przypisuje dane id do odpowiedniego elementów krajobrazu.
  * */
  int _id;
  public:
   /*! 
  * \brief Metoda virtualna sprawdzenia czy dron jest nad elementem. 
  * */
  virtual bool czy_nad(Interfejs_Drona* D)=0;
  /*! 
  * \brief Metoda virtualna sprawdzenia czy dron może lądować.
  * */
  virtual bool czy_ladowac(Interfejs_Drona* D)=0;
  /*! 
  * \brief Metoda virtualna do zmazania elemntów krajobrazu.
  * */
  virtual void zmaz()=0;
  /*! 
  * \brief Metoda get_id
  *
  * Metoda, która zwraca id elemntu krajobrazu.
  * \return zwraca id drona
  * */
int get_id1() {return _id;}
 /*! 
  * \brief Metoda virtualna do zdobycia środka figury
  * */
virtual Wektor<3> get_srodek_e()=0;
 /*! 
  * \brief Metoda virtualna do zdobycia wysokości figury
  * */
virtual double get_wysokosc()=0;
};
/*!
* \brief Modeluje układ współrzędnych 
*
* Klasa ta odpowiada za przypisanie środka, orientacji i poprzednika dronów i elementów krajobrazu 
* w układzie współrzędnych.
* Posiada metody, które odpowiadają za przesunięcia obiektów, rotacji i ustalenia ich położenia.
* Posiada również 3 parametry: poprzednik, środek i orientacja.
* */
class UkladW {
  protected:
  /*! 
  * \brief Poprzedni element
  *
  * Pole to zawiera wskaźnik do poprzedniego elementu.
  * */
  UkladW* poprzednik;
  /*! 
  * \brief Środek elementu
  *
  * Pole to zawiera współrzędne środka obiektu.
  * */
  Wektor<3> _srodek;
  /*! 
  * \brief Orientacja elementu
  *
  * Pole to zawiera orientacje elementu.
  * */
  MacierzRot<3> _orientacja;
  public:
  void Translacja(Wektor<3> W);  
  drawNS::Point3D konwertuj (const Wektor<3> &in) const; 
  void Rotacja(MacierzRot<3> M); 
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor ten odpowiada za dostęp do zmiennych i przypisanie wartości domyślnych.
  * */
  UkladW(Wektor<3> polozenie, MacierzRot<3> orientacja, UkladW* rodzic): poprzednik(rodzic), _srodek(polozenie), _orientacja(orientacja) {}; 
  UkladW(): poprzednik(NULL),_srodek({0,0,0}), _orientacja() {};
  UkladW przelicz_do_globalnego();
  Wektor<3> przelicz_punkt_do_rodzica(Wektor<3> punkt_lokalny);
  void obroc(double kat); 
/*! 
  * \brief Metoda get_srodek
  *
  * Metoda ta zwraca współrzędne środka obiektu w układzie współrzędnych.
  * \return zwraca współrzędne środka
  * */
  Wektor<3> get_srodek() {return _srodek;} 
  /*! 
  * \brief Metoda get_orientacja
  *
  * Metoda ta zwraca orientację obiektu w układzie współrzędnych.
  * \return zwraca orientację
  * */
  MacierzRot<3> get_orientacja() {return _orientacja;} 
  /*! 
  * \brief Metoda get_poprzednik
  *
  * Metoda ta zwraca wskaźnik do poprzedniego obiektu w układzie współrzędnych.
  * \return zwraca wskaźnik do poprzedniego obiektu
  * */
  UkladW* get_poprzednik() {return poprzednik;} 
  /*! 
  * \brief Metoda set_poprzednik
  *
  * Metoda ta przypisuje poprzednik obiektu w układzie współrzędnych.
  * \param[in,out] poprzednika - obiekt, który przypisujemy jako poprzednik
  * */
  void set_poprzednik(UkladW* poprzednika) {poprzednik=poprzednika;} 
};
/*!
* \brief Modeluje figurę Prostopadłościan 
*
* Klasa ta modeluje figurę Prostopadłościan, jako korpus drona. 
* Dziedziczy metody i zmienne z UkladW i Interfejs_rysowania.
* Posiada metody, które odpowiadają za rysowanie i ustawienie środka.
* Posiada również 3 parametry: szerokość, wysokość i głębokość Prostopadłościanu.
* */
class Prostopadloscian : public UkladW ,  public Interfejs_rysowania {
  private:
  /*! 
  * \brief Szerokość Prostopadłościanu
  * */
  double _szerokosc;
  /*! 
  * \brief Wysokość Prostopadłościanu
  * */
  double _wysokosc;
  /*! 
  * \brief Głębokość Prostopadłościanu
  * */
  double _glebokosc;
  public:
  void rysuj() override; 
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor ten odpowiada za  przypisanie wartości do zmiennych.
  * */
  Prostopadloscian(Wektor<3> polozenie, MacierzRot<3> orientacja, UkladW* rodzic, double szerokosc, double wysokosc, double glebokosc, drawNS::Draw3DAPI *rysownik): //konstruktor o danym połozeniu, orientacji i parametrach
  UkladW(polozenie,orientacja, rodzic),Interfejs_rysowania(rysownik), _szerokosc(szerokosc), _wysokosc(wysokosc), _glebokosc(glebokosc) {};
  /*! 
  * \brief Konstruktor domyslny
  * */
  Prostopadloscian() {};
  /*! 
  * \brief Metoda set_srodek
  *
  * Metoda ta przypisuje srodek obiektu w układzie współrzędnych.
  * \param[in,out] poprzednika - przypisanie srodka obiektu
  * */
  void set_srodek(Wektor<3> srod) {_srodek=srod;} 
   /*! 
  * \brief Metoda get_wysokosc
  *
  * Metoda ta zwraca wysokość prostopadłościanu.
  * \return zwraca wysokość prostopadłościanu
  * */
  double get_wysokosc1() {return _wysokosc;}
   /*! 
  * \brief Metoda get_glebokosc
  *
  * Metoda ta zwraca głębokość płaskozwyża.
  * \return zwraca głębokość płaskozwyża
  * */
  double get_glebokosc() {return _glebokosc;}
};

/*!
* \brief Modeluje figurę Graniastosłup sześciokątny 
*
* Klasa ta modeluje figurę Graniastosłup, jako wirnik drona. 
* Dziedziczy metody i zmienne z UkladW i Interfejs_rysowania.
* Posiada metody, które odpowiadają za rysowanie i ustawienie orientacji.
* Posiada również 2 parametry:  wysokość i długośc boku Graniastosłupa.
* */
class Graniastoslup6 : public UkladW, public Interfejs_rysowania {
  private:
  /*! 
  * \brief Wysokość Graniastosłupa
  * */
  double _wysokosc;
  /*! 
  * \brief Długość boku Graniastosłupa
  * */
  double _dlugosc_boku;
  public:
  void rysuj() override; 
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor ten odpowiada za przypisanie wartości do zmiennych.
  * */
  Graniastoslup6(Wektor<3> polozenie, MacierzRot<3> orientacja, UkladW* rodzic, double wysokosc, double dlugosc_boku,drawNS::Draw3DAPI *rysownik, int id=-1): 
  UkladW(polozenie,orientacja, rodzic),Interfejs_rysowania(rysownik), _wysokosc(wysokosc), _dlugosc_boku(dlugosc_boku) {this->id=id;};
   /*! 
  * \brief Metoda set_orientacja
  *
  * Metoda ta przypisuje orientację danego obiektu.
  * \param[in,out] ori - przypisanie orientacji obiektu
  * */
  void set_orientacja(MacierzRot<3> ori) {_orientacja=ori;} 
};
/*!
* \brief Modeluje drona 
*
* Klasa ta modeluje drona, składającego się z korpusa i wirników. 
* Dziedziczy metody i zmienne z UkladW, Interfejs_el_krajobrazu, Interfejs_Drona i Interfejs_rysowania.
* Posiada metody, które odpowiadają za rysowanie, ruch, obrót, lądowanie i zmazanie drona.
* Posiada również 3 parametry: korpus, wirniki i poprzednia pozycja.
* */
class Dron : protected UkladW , public Interfejs_rysowania, public Interfejs_el_krajobrazu, public Interfejs_Drona { 
  private:
  /*! 
  * \brief Korpus drona jako Prostopadłościan
  * */
  Prostopadloscian korpus;
  /*! 
  * \brief wrniki drona jako Graniastosłupy
  * */
  std::vector<Graniastoslup6> wirniki;
  /*! 
  * \brief Poprzednia pozycja środka drona
  * */
  Wektor<3> poprzednia_pozycja;
  public:
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor ten odpowiada za przypisanie wartości do zmiennych.
  * */
  Dron(Wektor<3> polozenie, MacierzRot<3> orientacja, UkladW* rodzic, double szerokosc, double wysokosc, double glebokosc, drawNS::Draw3DAPI *rysownik): UkladW(Wektor<3>({0,0,0}),MacierzRot<3> (0, 'Z'), rodzic),Interfejs_rysowania(rysownik),
  korpus(polozenie,orientacja,this, szerokosc, wysokosc, glebokosc, rysownik), 
  wirniki({Graniastoslup6(Wektor<3>({-(szerokosc)/2,-glebokosc/2,+wysokosc}),MacierzRot<3> (0, 'Z'),&korpus,wysokosc*0.1,std::min(szerokosc,glebokosc)*0.5,rysownik),
  Graniastoslup6(Wektor<3>({-(szerokosc)/2,+glebokosc/2,+wysokosc}),MacierzRot<3> (0, 'Z'),&korpus,wysokosc*0.1,std::min(szerokosc,glebokosc)*0.5,rysownik),
  Graniastoslup6(Wektor<3>({+(szerokosc)/2,+glebokosc/2,+wysokosc}),MacierzRot<3> (0, 'Z'),&korpus,wysokosc*0.1,std::min(szerokosc,glebokosc)*0.5,rysownik),
  Graniastoslup6(Wektor<3>({+(szerokosc)/2,-glebokosc/2,+wysokosc}),MacierzRot<3> (0, 'Z'),&korpus,wysokosc*0.1,std::min(szerokosc,glebokosc)*0.5,rysownik)}) {};
  void rysuj() override; 
  void lec_w_przod(double ile) override; 
  void lec_w_gore(double ile) override; 
  void obroc(double kat) override;  
  void obroc_wirniki() override; 
  void rysuj_linie() override; 
  /*! 
  * \brief Metoda get_srodek
  *
  * Metoda ta zwraca współrzędne środka korpusu drona w układzie współrzędnych.
  * \return zwraca współrzędne środka korpusu drona
  * */
  Wektor<3> get_srodek2() override {return korpus.get_srodek();} 
  /*! 
  * \brief Metoda get_srodek
  *
  * Metoda ta zwraca współrzędne środka korpusu drona jako elementu krajobrazu w układzie współrzędnych.
  * \return zwraca współrzędne środka korpusu drona
  * */
  Wektor<3> get_srodek_e() override {return korpus.get_srodek();} 
  void animuj(double gora, double obrot, double przod, Scena Sc) override;
  bool czy_nad(Interfejs_Drona* D) override;
  bool czy_ladowac(Interfejs_Drona* D) override;
  /*! 
  * \brief Metoda get_glebokosc
  *
  * Metoda ta zwraca glebokość drona(korpusu)
  * \return zwraca glebokość korpusu drona
  * */
  double get_glebokosc1() override {return korpus.get_glebokosc();}
  /*! 
  * \brief Metoda ta zmazuje korpus i wirniki drona o danym id 
  * */
  void zmaz_drona() override {rysownik->erase_shape(korpus.get_id());
    rysownik->erase_shape(wirniki[0].get_id());
    rysownik->erase_shape(wirniki[1].get_id());
    rysownik->erase_shape(wirniki[2].get_id());
    rysownik->erase_shape(wirniki[3].get_id());};
    void zmaz() override {};
     /*! 
  * \brief Metoda get_wysokosc
  *
  * Metoda ta zwraca wysokość drona.
  * \return zwraca wysokość drona
  * */
    double get_wysokosc() override {return 1;};
};

/*!
* \brief Modeluje Powierzchnie 
*
* Klasa ta modeluje powierzchnie, na której znajdują się elementy. 
* Dziedziczy metody i zmienne z Interfejs_rysowania.
* Posiada metodę, które odpowiadają za rysowanie.
* Posiada również  parametr: wysokość.
* */
class Powierzchnia: public Interfejs_rysowania {
  private:
  /*! 
  * \brief Wysokość, na której jest powierzchnia 
  * */
  double _wysokosc;
  public:
  void rysuj() override; 
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor ten odpowiada za przypisanie wartości do zmiennych.
  * */
  Powierzchnia(double wysokosc, drawNS::Draw3DAPI *rysownik): Interfejs_rysowania(rysownik), _wysokosc(wysokosc) {}; 
};
/*!
* \brief Modeluje figurę Płaskozwyżu  
*
* Klasa ta modeluje figurę Płaskozwyżu, jako elementu krajobrazu. 
* Dziedziczy metody i zmienne z UkladW, Interfejs_el_krajobrazu i Interfejs_rysowania.
* Posiada metody, które odpowiadają za rysowanie, zmazywanie i lądowanie.
* Posiada również 2 parametr:  wierzchołki i wysokość figury.
* */
class Plaskozwyz: protected UkladW, public Interfejs_rysowania, public Interfejs_el_krajobrazu {
  private:
  /*! 
  * \brief Poszczególne wierzchołki Płaskozwyżu
  * */
   std::vector<drawNS::Point3D> _wierzcholki;
   /*! 
  * \brief Wysokość Płaskozwyżu
  * */
  double _wysokosc;
   /*! 
  * \brief Losowa odległość wierzchołków od środka Płaskozwyżu
  * */
  int odleglosc=rand()%7+3;
  public:
  bool czy_nad(Interfejs_Drona* D) override;
  bool czy_ladowac(Interfejs_Drona* D) override; 
  void rysuj() override;
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor ten odpowiada za przypisanie wartości do zmiennych.
  * */
  Plaskozwyz(Wektor<3> polozenie, MacierzRot<3> orientacja, UkladW* rodzic, double wysokosc, drawNS::Draw3DAPI *rysownik):
  UkladW(polozenie,orientacja, rodzic),Interfejs_rysowania(rysownik), _wysokosc(wysokosc) {}   
  /*! 
  * \brief Metoda ta zmazuje Płaskozwyż o danym id
  * */
  void zmaz() override {rysownik->erase_shape(get_id1());}
  /*! 
  * \brief Metoda get_srodek
  *
  * Metoda ta zwraca współrzędne środka płaskozwyża.
  * \return zwraca współrzędne środka płaskozwyża.
  * */
  Wektor<3> get_srodek_e() override {return _srodek;}
   /*! 
  * \brief Metoda get_wysokosc
  *
  * Metoda ta zwraca wysokość płaskozwyża.
  * \return zwraca wysokość płaskozwyża
  * */
  double get_wysokosc() override {return _wysokosc;}
};
/*!
* \brief Modeluje figurę Wzgórza  
*
* Klasa ta modeluje figurę Wzgórza, jako elementu krajobrazu. 
* Dziedziczy metody i zmienne z UkladW, Interfejs_el_krajobrazu i Interfejs_rysowania.
* Posiada metody, które odpowiadają za rysowanie, zmazywanie i lądowanie.
* Posiada również 2 parametr:  wierzchołki i wysokość figury.
* */
class Wzgorze: protected UkladW, public Interfejs_rysowania, public Interfejs_el_krajobrazu {
  private:
  /*! 
  * \brief Poszczególne wierzchołki Wzgórza
  * */
  std::vector<drawNS::Point3D> _wierzcholki;
  /*! 
  * \brief Wysokość Wzgórza
  * */
  double _wysokosc;
   /*! 
  * \brief Losowa odległość wierzchołków od środka Wzgórza
  * */
  int odleglosc=rand()%7+3;
  public:
  bool czy_nad(Interfejs_Drona*) override;
  bool czy_ladowac(Interfejs_Drona*) override;
  void rysuj() override;
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor ten odpowiada za przypisanie wartości do zmiennych.
  * */
  Wzgorze(Wektor<3> polozenie, MacierzRot<3> orientacja, UkladW* rodzic,  double wysokosc, drawNS::Draw3DAPI *rysownik):
  UkladW(polozenie,orientacja, rodzic),Interfejs_rysowania(rysownik), _wysokosc(wysokosc) {};
   /*! 
  * \brief Metoda ta zmazuje Wzgórze o danym id
  * */
  void zmaz() override {rysownik->erase_shape(get_id1());}
  /*! 
  * \brief Metoda get_srodek
  *
  * Metoda ta zwraca współrzędne środka wzgórza.
  * \return zwraca współrzędne środka wzgórza.
  * */
  Wektor<3> get_srodek_e() override {return _srodek;}
  /*! 
  * \brief Metoda get_wysokosc
  *
  * Metoda ta zwraca wysokość wzgórza.
  * \return zwraca wysokość wzgórza
  * */
  double get_wysokosc() override {return _wysokosc;}
  };

/*!
* \brief Modeluje figurę Płaskozwyżu Prostego  
*
* Klasa ta modeluje figurę Płaskozwyżu Prostego, jako elementu krajobrazu. 
* Dziedziczy metody i zmienne z Prostopadłościanu i Interfejs_el_krajobrazu.
* Posiada metody, które odpowiadają za zmazywanie i lądowanie.
* */
class Plaskozwyz_prost: public Prostopadloscian, public Interfejs_el_krajobrazu {
  public:
  bool czy_nad(Interfejs_Drona* D) override;
  bool czy_ladowac(Interfejs_Drona* D) override ;
  /*! 
  * \brief Konstruktor
  *
  * Konstruktor ten odpowiada za przypisanie wartości do zmiennych.
  * */
  Plaskozwyz_prost(Wektor<3> polozenie, MacierzRot<3> orientacja, UkladW* rodzic, double szerokosc, double wysokosc, double glebokosc, drawNS::Draw3DAPI *rysownik):
  Prostopadloscian(polozenie, orientacja, rodzic, szerokosc, wysokosc, glebokosc,rysownik) {};
  /*! 
  * \brief Metoda ta zmazuje Płaskozwyż Prosty o danym id
  * */
  void zmaz() override {rysownik->erase_shape(get_id1());}
  /*! 
  * \brief Metoda zwracająca środek bryły
  *
  * \return zwraca środek bryły
  * */
  Wektor<3> get_srodek_e() override {return _srodek;}
   /*! 
  * \brief Metoda get_wysokosc
  *
  * Metoda ta zwraca wysokość płaskozwyża prostego.
  * \return zwraca wysokość płaskozwyża prostego
  * */
  double get_wysokosc() override {return get_wysokosc1();};
};
/*!
* \brief Modeluje Scenę  
*
* Klasa ta modeluje Scenę, na której znajdują się wszystkie elementy. 
* Posiada metody, które odpowiadają za wyświetlanie, dodawanie i usuwanie elementów oraz dostepu do odpowiednich zmiennych.
* Posiada 3 parametry - od 3 interfejsów 
* */
class Scena {
  protected:
  /*! 
  * \brief Vector zawierający elementy od Interfejsu rysowania
  * */
  std::vector<std::shared_ptr<Interfejs_rysowania>> Ir;
  /*! 
  * \brief Vector zawierający elementy od Interfejsu Drona
  * */
  std::vector<std::shared_ptr<Interfejs_Drona>> Id;
  /*! 
  * \brief Vector zawierający elementy od Interfejsu elementów krajobrazu
  * */
  std::vector<std::shared_ptr<Interfejs_el_krajobrazu>> Ie;
  public:
  void pokaz();
  void pokaz_drony();
  void pokaz_el_kraj();
  void rysuj_wszystkie_elementy(std::vector<Interfejs_rysowania*> elementy);
  void dodaj_drona(drawNS::Draw3DAPI *rysownik, Wektor<3> wek, MacierzRot<3> mac, double szerokosc, double wysokosc, double glebokosc);
  void dodaj_el_kraj(drawNS::Draw3DAPI *rysownik,Wektor<3> wek, MacierzRot<3> mac, double szerokosc, double wysokosc, double glebokosc);
  void usun_drona(int ide);
  void usun_el_kraj(int ide);
  /*! 
  * \brief Metoda get_Ir
  *
  * Metoda ta zwraca ostatni element z listy Interfejsu rysowania
  * \return zwraca ostatni element z listy Interfejsu rysowania
  * */
  Interfejs_rysowania* get_Ir() {return Ir.back().get();}
  /*! 
  * \brief Metoda get_Ir_size
  *
  * Metoda ta zwraca rozmiar listy Ir
  * \return zwraca rozmiar listy Ir
  * */
  uint get_Ir_size() {return Ir.size();} 
  /*! 
  * \brief Metoda get_Ir
  *
  * Metoda ta zwraca ostatni element z listy Interfejsu Drona
  * \return zwraca ostatni element z listy Interfejsu Drona
  * */
  Interfejs_Drona* get_Id() {return Id.back().get();}
  /*! 
  * \brief Metoda get_Ir
  *
  * Metoda ta zwraca ostatni element z listy Interfejsu elementów krajobrazu
  * \return zwraca ostatni element z listy Interfejsu elementów krajobrazu
  * */
  Interfejs_el_krajobrazu* get_Ie() {return Ie.back().get();}
  /*! 
  * \brief Metoda get_Ir_size
  *
  * Metoda ta zwraca rozmiar listy Id
  * \return zwraca rozmiar listy Id
  * */
  uint get_Id_size() {return Id.size();} 
  /*! 
  * \brief Metoda get_Ir_size
  *
  * Metoda ta zwraca rozmiar listy Ie
  * \return zwraca rozmiar listy Ie
  * */
  uint get_Ie_size() {return Ie.size();} 
  /*! 
  * \brief Metoda get_Id(ide)
  *
  * Metoda ta zwraca dany element o odpowiednim indeksie z listy Id
  * \param[in] ide - indeks
  * \return zwraca rozmiar listy Ir
  * */
  std::shared_ptr<Interfejs_Drona> get_Id(uint ide) {return Id[ide];}
  /*! 
  * \brief Metoda get_Id(ide)
  *
  * Metoda ta zwraca dany element o odpowiednim indeksie z listy Ir
  * \param[in] ide - indeks
  * \return zwraca rozmiar listy Ir
  * */
  std::shared_ptr<Interfejs_rysowania> get_Ir(uint ide) {return Ir[ide];}
  /*! 
  * \brief Metoda get_Id(ide)
  *
  * Metoda ta zwraca dany element o odpowiednim indeksie z listy Ie
  * \param[in] ide - indeks
  * \return zwraca rozmiar listy Ir
  * */
  std::shared_ptr<Interfejs_el_krajobrazu> get_Ie(uint ide) {return Ie[ide];}
  
};









#endif
