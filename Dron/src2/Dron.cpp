#include "../inc2/Dron.hh"
#include <cstdlib>
#include <math.h>
#include <thread>
#include <chrono>

/*! 
* \file
* \brief Definicja metod klasy Prostopadloscian,Graniastoslup6,Dron,
*UkladW,Powierzchnia,Plaskozwyz,Wzgorze,Plaskozwyz_prost i Scena
*
* Zawiera definicje metod klasy Prostopadloscian,Graniastoslup6,Dron,
*UkladW,Powierzchnia,Plaskozwyz,Wzgorze,Plaskozwyz_prost i Scena
*/

/*! 
* \brief Przesunięcie środka obiektu
*
* Przesuwa środek obiektu według danego wektora
* \param[in] W - wektor o który przesuwamy obiekt
*/
void UkladW::Translacja(Wektor<3> W) {
    _srodek= _srodek +W;
}
/*! 
* \brief Obrót obiektu
*
* Obraca obiekt według danej macierzy
* \param[in] M - macierz obrotu 
*/
void UkladW::Rotacja(MacierzRot<3> M) {
    _orientacja= _orientacja * M;
}
/*! 
* \brief konwersja punktów
*
* Konwertuje punkty na punkty na rysunku w GNUplocie
* \param[in] in - wektor, który zamieniamy na punkty w GNUplocie
* \return zwraca odpowiednie współrzędne na rysunku
*/
 drawNS::Point3D UkladW::konwertuj(const Wektor<3> &in) const {
return drawNS::Point3D(in[0],in[1],in[2]);
}
/*! 
* \brief Przeliczenie do pierwszego obiektu
*
* Przelicza obiekt do globalnego. Zwraca środek, orientacje i wskaźnik poprzednika
* według nowego punktu w układzie współrzędnych
* \return zwraca nowy punkt, orientacje i wskaźnik w układzie współrzędnym
*/
UkladW UkladW::przelicz_do_globalnego() { //zwraca obiekt
   UkladW punkt(*this);
   if (punkt.poprzednik==nullptr) {return punkt;}
    while (punkt.poprzednik->poprzednik!=nullptr) {
        punkt._srodek=punkt.przelicz_punkt_do_rodzica(_srodek);
        punkt._orientacja=punkt.poprzednik->_orientacja * punkt._orientacja;
        punkt.poprzednik=punkt.poprzednik->poprzednik;
    }
    return punkt; 
}
/*! 
* \brief Przeliczenie do poprzednika
*
* Ustawia nowy punkt względem rodzica
* \param[in] punkt_lokalny - podajemy punkt, który chcemy zmienić
* \return zwraca nowy punkt w układzie współrzędnym
*/
Wektor<3> UkladW::przelicz_punkt_do_rodzica(Wektor<3> punkt_lokalny) {
    return poprzednik->_orientacja* punkt_lokalny + poprzednik->_srodek; //punkt wzgledem rodzica
}
/*! 
* \brief Rysowanie Prostopadłościanu
*
* Rysuje Prostopadłościan(korpus) w GNUplocie o danej wysokości, szerokości i głębokości
*/
void Prostopadloscian::rysuj() {
    std::vector<std::vector<drawNS::Point3D>> vtmp;
    std::vector<drawNS::Point3D> ltmp;
    UkladW globalny; 
    globalny=przelicz_do_globalnego();
    rysownik->erase_shape(id);
    ltmp.push_back(konwertuj(globalny.get_srodek()+globalny.get_orientacja()*(Wektor<3>({-(_szerokosc)/2,-_glebokosc/2,0}))));
    ltmp.push_back(konwertuj(globalny.get_srodek()+globalny.get_orientacja()*(Wektor<3>({-(_szerokosc)/2,+_glebokosc/2,0}))));
    ltmp.push_back(konwertuj(globalny.get_srodek()+globalny.get_orientacja()*(Wektor<3>({+(_szerokosc)/2,+_glebokosc/2,0}))));
    ltmp.push_back(konwertuj(globalny.get_srodek()+globalny.get_orientacja()*(Wektor<3>({+(_szerokosc)/2,-_glebokosc/2,0}))));
    vtmp.push_back(ltmp);
    ltmp.pop_back();
    ltmp.pop_back();
    ltmp.pop_back();
    ltmp.pop_back();
    ltmp.push_back(konwertuj(globalny.get_srodek()+globalny.get_orientacja()*(Wektor<3>({-(_szerokosc)/2,-_glebokosc/2,+_wysokosc}))));
    ltmp.push_back(konwertuj(globalny.get_srodek()+globalny.get_orientacja()*(Wektor<3>({-(_szerokosc)/2,+_glebokosc/2,+_wysokosc}))));
    ltmp.push_back(konwertuj(globalny.get_srodek()+globalny.get_orientacja()*(Wektor<3>({+(_szerokosc)/2,+_glebokosc/2,+_wysokosc}))));
    ltmp.push_back(konwertuj(globalny.get_srodek()+globalny.get_orientacja()*(Wektor<3>({+(_szerokosc)/2,-_glebokosc/2,+_wysokosc}))));
    vtmp.push_back(ltmp);
    id=rysownik->draw_polyhedron(vtmp, "purple");

}
/*! 
* \brief Rysowanie Granistosłupu
*
* Rysuje Granistosłup(wirnik) w GNUplocie o danej wysokości i długości boku
*/
    void Graniastoslup6::rysuj() {
    std::vector<std::vector<drawNS::Point3D>> vtmp;
    std::vector<drawNS::Point3D> ltmp;
    UkladW globalny=przelicz_do_globalnego();
    rysownik->erase_shape(id);
    double s3d2=sqrt(3)/2;
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({0,+_dlugosc_boku,0})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({+_dlugosc_boku*s3d2,+_dlugosc_boku/2,0})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({+_dlugosc_boku*s3d2,-_dlugosc_boku/2,0})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({0,-_dlugosc_boku,0})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({-_dlugosc_boku*s3d2,-_dlugosc_boku/2,0})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({-_dlugosc_boku*s3d2,+_dlugosc_boku/2,0})+globalny.get_srodek()));
    vtmp.push_back(ltmp);
    ltmp.pop_back();
    ltmp.pop_back();
    ltmp.pop_back();
    ltmp.pop_back();
    ltmp.pop_back();
    ltmp.pop_back();
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({0,+_dlugosc_boku,+_wysokosc})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({+_dlugosc_boku*s3d2,+_dlugosc_boku/2,+_wysokosc})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({+_dlugosc_boku*s3d2,-_dlugosc_boku/2,+_wysokosc})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({0,-_dlugosc_boku,+_wysokosc})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({-_dlugosc_boku*s3d2,-_dlugosc_boku/2,+_wysokosc})+globalny.get_srodek()));
    ltmp.push_back(konwertuj(_orientacja*Wektor<3>({-_dlugosc_boku*s3d2,+_dlugosc_boku/2,+_wysokosc})+globalny.get_srodek()));
    vtmp.push_back(ltmp);
    id=rysownik->draw_polyhedron(vtmp, "purple");
}
/*! 
* \brief Rysowanie Drona
*
* Rysuje Drona w GNUplocie o danym korpusie i wirnikach
*/
   void Dron::rysuj() {
    korpus.rysuj();
    wirniki[0].rysuj();
    wirniki[1].rysuj();
    wirniki[2].rysuj();
    wirniki[3].rysuj();
}
/*! 
* \brief Rysowanie Powierzchni
*
* Rysuje Powierzchnię w GNUplocie na danej wysokości
*/
void Powierzchnia::rysuj() {
    std::vector<std::vector<drawNS::Point3D>> vtmp;
    std::vector<drawNS::Point3D> ltmp;
    for (int i=-20;i<20;i++ ) {
        for (int j=-20;j<20;j++) { 
        ltmp.push_back({{drawNS::Point3D(j,i,_wysokosc)}});       
        }  
        vtmp.push_back(ltmp);  
        for (int j=-20;j<20;j++) {
            ltmp.pop_back();
        }   
    }
    rysownik->draw_surface(vtmp, "grey");
    
}
/*! 
* \brief Rysowanie linii
*
* Rysuje linię ruchu drona 
*/
void Dron::rysuj_linie() {    
    rysownik->draw_line(konwertuj(poprzednia_pozycja),konwertuj(korpus.get_srodek()), "green");
}
/*! 
* \brief Ruch w góre
*
* Wykonuje ruch drona w górę o daną wartość
* \param[in] ile - dana wartość z jaką dron się porusza
*/
void Dron::lec_w_gore(double ile) {
    poprzednia_pozycja=korpus.przelicz_do_globalnego().get_srodek();
    korpus.set_srodek(Wektor<3>({korpus.get_srodek()[0],korpus.get_srodek()[1],korpus.get_srodek()[2]+ile}));
}
/*! 
* \brief Ruch w przód
*
* Wykonuje ruch drona w przód o daną wartość
* \param[in] ile - dana wartość z jaką dron się porusza
*/
void Dron::lec_w_przod(double ile) {
    poprzednia_pozycja=korpus.przelicz_do_globalnego().get_srodek();
    Wektor<3> tmpl ({0,1,0}); // wektor, przesuniecie o z
    korpus.set_srodek(korpus.get_srodek()+(korpus.get_orientacja()*tmpl) *ile);  //wywolanie tej funkcji 
}
/*! 
* \brief Obrót
*
* Odwołanie się do funkcji w UkladW, w celu obrócenia drona
* \param[in] kat - dana wartość o jaką dron się obróci
*/
void Dron::obroc(double kat) {
    korpus.obroc(kat); 
}
/*! 
* \brief Obróć
*
* Obrót korpusu drona o daną wartość
* \param[in] kat - dana wartość o jaką dron się obróci
*/
void UkladW::obroc(double kat) { 
    _orientacja=_orientacja*MacierzRot<3>(kat, 'Z');
}
/*! 
* \brief Obróć wirniki
*
* Obrót wrników drona w swojej osi
*/
void Dron::obroc_wirniki() { 
    for (int i=0; i<4; i++) {
        wirniki[i].set_orientacja(MacierzRot<3>(20,'Z')*wirniki[i].get_orientacja());
    }
    
}
/*! 
* \brief Animacja
*
* Animacja ruchu drona o wartości nadane od użytkownika. 
* Odwołanie do metod ruchu w przód i w góre.
* \param[in] gora - dana wartość z jaką dron się porusza w górę
* \param[in] obrot - dana wartość o jaką dron się obróci
* \param[in] przod - dana wartość z jaką dron się porusza w przod
* \param[in] Sc - obiekt sceny, wykorzystywany do odwołania się do figur
*/
void Dron::animuj(double gora, double obrot, double przod, Scena Sc) { //animacja drona
    const int lklatek =30;
    for (int i=0;i<lklatek;i++) {
        lec_w_gore((double)1/lklatek*gora);
        obroc_wirniki();
        rysuj_linie();
        rysuj();
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    for (int i=0;i<lklatek;i++) {
        obroc((double)1/lklatek*obrot);
        obroc_wirniki();
        rysuj_linie();
        rysuj();
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    for (int i=0;i<lklatek;i++) {
        lec_w_przod((double)1/lklatek*przod);
        obroc_wirniki();
        rysuj_linie();
        rysuj();
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    for (uint i=0;i<Sc.get_Ie_size();i++) {
        if(Sc.get_Ie(i)->czy_nad(this)==true) { 
        std::cout << "Dron jest nad elementem";
        break;
        }
    }
    bool do_przodu=true;
    int indeks=-1;
    while(do_przodu==true) {
        bool nad_niczym=true;
        for(uint i=0; i<Sc.get_Ie_size(); i++) { 
            if(Sc.get_Ie(i)->czy_ladowac(this)==false&&Sc.get_Ie(i)->czy_nad(this)==true) { 
            do_przodu=true;
            }
            else if(Sc.get_Ie(i)->czy_ladowac(this)==true&&Sc.get_Ie(i)->czy_nad(this)==true) {do_przodu=false; indeks=i;}
            if(Sc.get_Ie(i)->czy_nad(this)==true) {nad_niczym=false;}
        }
        if(nad_niczym==true) {do_przodu=false;}
        lec_w_przod((double)1/lklatek*przod);
        obroc_wirniki();
        rysuj_linie();
        rysuj();
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    if(indeks==-1) { 
    for (int i=0;i<lklatek;i++) {
        lec_w_gore((double)-1/lklatek*gora);
        obroc_wirniki();
        rysuj_linie();
        rysuj();
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    }
    else {
        for (int i=0;i<lklatek;i++) {
        lec_w_gore((double)-1/lklatek*(gora-Sc.get_Ie(indeks)->get_wysokosc()));
        obroc_wirniki();
        rysuj_linie();
        rysuj();
        std::this_thread::sleep_for(std::chrono::milliseconds(200));

    }
    }
}
/*! 
* \brief Rysowanie Płaskozwyżu
*
* Rysuje Płaskozwyż w GNUplocie o danej wysokości, losowej liczbie wierzchołków i odległości od środka
*/
void Plaskozwyz::rysuj() {
    std::vector<std::vector<drawNS::Point3D>> vtmp;
    std::vector<drawNS::Point3D> ltmp;
   rysownik->erase_shape(id);
   int liczba_wierzcholkow = rand()%7+3;
   for (int i=0; i<liczba_wierzcholkow; i++) { 
    _wierzcholki.push_back(konwertuj(Wektor<3>({(double)odleglosc*sin((double)i/liczba_wierzcholkow*2*M_PI),(double)odleglosc*cos((double)i/liczba_wierzcholkow*2*M_PI),0})+get_srodek()));
   }
    vtmp.push_back(_wierzcholki);
    ltmp=_wierzcholki;
    for (uint i=0; i<ltmp.size();i++) {
        ltmp[i][2]+=_wysokosc;
    }
    vtmp.push_back(ltmp);
    id=rysownik->draw_polyhedron(vtmp,"green");
}
/*! 
* \brief Rysowanie Wzgórza
*
* Rysuje Wzgórze w GNUplocie o danej wysokości, losowej liczbie wierzchołków i odległości od środka
*/
void Wzgorze::rysuj() {
    std::vector<std::vector<drawNS::Point3D>> vtmp;
    std::vector<drawNS::Point3D> ltmp;
    rysownik->erase_shape(id);
  int liczba_wierzcholkow = rand()%7+3;
  for (int i=0; i<liczba_wierzcholkow; i++) { 
    _wierzcholki.push_back(konwertuj(Wektor<3>({(double)odleglosc*sin((double)i/liczba_wierzcholkow*2*M_PI),(double)odleglosc*cos((double)i/liczba_wierzcholkow*2*M_PI),0})+get_srodek()));
  }   
    vtmp.push_back(_wierzcholki);
    ltmp=_wierzcholki;
    for (uint i=0; i<ltmp.size();i++) {
        ltmp[i][2]+=_wysokosc;
        ltmp[i][1]=_srodek[1];
        ltmp[i][0]=_srodek[0];
    }
    vtmp.push_back(ltmp);
    id=rysownik->draw_polyhedron(vtmp,"green");
}
/*! 
* \brief Rysowanie 
*
* Odwołanie się do funckji rysowania.
* Rysuje wszystkie elementy w zbiorze
* \param[in,out] elementy - lista wskaźników Interfejsu rysowania, rysuje te wszystkie elementy
*/
void Scena::rysuj_wszystkie_elementy(std::vector<Interfejs_rysowania*> elementy) {
    for( uint i=0; i<elementy.size();i++) {
        elementy[i]->rysuj();
    }
}

/*! 
* \brief Wyświetlanie
*
* Pokazuje środki wszystkich elementów w zbiorze Interfejsu rysowania
*/
void Scena::pokaz() {
    for (uint i=0; i<Ir.size();i++) {
        std::cout <<"Id: " << Ir[i] << std::endl;
    }
}
/*! 
* \brief Wyświetlanie
*
* Pokazuje środki wszystkich elementów w zbiorze Interfejsu drona
*/
void Scena::pokaz_drony() {
    for (uint i=0; i<Id.size();i++) {
        std::cout << "Dron Id-  " << i << ": "  << Id[i]->get_srodek2() << std::endl;
    }
}
/*! 
* \brief Wyświetlanie
*
* Pokazuje środki wszystkich elementów w zbiorze Interfejsu elemntów krajobrazu
*/
void Scena::pokaz_el_kraj() {
    for (uint i=0; i<Ie.size();i++) {
        std::cout << "Elementy Id- " << i << ": " << Ie[i]->get_srodek_e() << std::endl;
    }
}
/*! 
* \brief Dodaj drona
*
* Tworzy nowego drona, dodaje do rysownika i list, przy podanych parametrach
* \param[in,out] rysownik - rysownik w GNUplocie
* \param[in] wek - środek nowo powstałego drona
* \param[in] mac - orientacja nowo powstałego drona
* \param[in] szerokosc- szerokość nowo powstałego drona
* \param[in] wysokosc - wysokość nowo powstałego drona
* \param[in] glebokosc - głębokość nowo powstałego drona
*/
void Scena::dodaj_drona(drawNS::Draw3DAPI *rysownik, Wektor<3> wek, MacierzRot<3> mac, double szerokosc, double wysokosc, double glebokosc) {
     Id.push_back(std::shared_ptr<Interfejs_Drona>(new Dron(wek,mac,NULL,szerokosc,wysokosc,glebokosc,rysownik)));
     Ie.push_back(std::shared_ptr<Interfejs_el_krajobrazu>(new Dron(wek,mac,NULL,szerokosc,wysokosc,glebokosc,rysownik)));
     Ir.push_back(std::dynamic_pointer_cast<Interfejs_rysowania>(Id.back()));
}
/*! 
* \brief Dodaj elementy krajobrazu
*
* Tworzy nowy element krajobrazu, który chcemy, dodaje do rysownika i list, przy podanych parametrach
* \param[in,out] rysownik - rysownik w GNUplocie
* \param[in] wek - środek nowo powstałego elementu krajobrazu
* \param[in] mac - orientacja nowo powstałego elementu krajobrazu
* \param[in] szerokosc- szerokość nowo powstałego elementu krajobrazu
* \param[in] wysokosc - wysokość nowo powstałego elementu krajobrazu
* \param[in] glebokosc - głębokość nowo powstałego elementu krajobrazu
*/
void Scena::dodaj_el_kraj(drawNS::Draw3DAPI *rysownik,Wektor<3> wek, MacierzRot<3> mac, double szerokosc, double wysokosc, double glebokosc) {
    int wybor;
    while (wybor!=1 && wybor!=2 && wybor!=3) { 
        std:: cout << "Ktory element krajobrazu chcesz dodac: " << std::endl 
        <<"1.Plaskozwyz" << std::endl
        <<"2.Wzgorze" << std::endl
        <<"3.Plaskozwyz prosty" << std::endl;
        std:: cin >> wybor;
    switch(wybor) {
        case 1: 
        Ie.push_back(std::shared_ptr<Interfejs_el_krajobrazu>(new Plaskozwyz(wek,mac,NULL,wysokosc,rysownik)));
        Ir.push_back(std::dynamic_pointer_cast<Interfejs_rysowania>(Ie.back()));
        break;
        case 2: 
        Ie.push_back(std::shared_ptr<Interfejs_el_krajobrazu>(new Wzgorze(wek,mac,NULL,wysokosc,rysownik)));
        Ir.push_back(std::dynamic_pointer_cast<Interfejs_rysowania>(Ie.back()));
        break;
        case 3: 
        Ie.push_back(std::shared_ptr<Interfejs_el_krajobrazu>(new Plaskozwyz_prost(wek,mac,NULL,szerokosc,wysokosc,glebokosc,rysownik)));
        Ir.push_back(std::dynamic_pointer_cast<Interfejs_rysowania>(Ie.back()));
        break;
        default:
        std::cout<<"Nie ma takiej opcji. Sprobuj ponownie" << std::endl;
    }
    }

}
/*! 
* \brief Usuń drona
*
* Usuwa drona o danym id z list 
* \param[in] ide - indeks drona
*/
void Scena::usun_drona(int ide) {
    std::shared_ptr<Interfejs_Drona> tmp = Id[ide];
    std::shared_ptr<Interfejs_rysowania> ltmp = std::reinterpret_pointer_cast<Interfejs_rysowania>(tmp);
    Ir.erase(std::remove(Ir.begin(),Ir.end(),ltmp),Ir.end());
    Id.erase(std::remove(Id.begin(),Id.end(),tmp),Id.end());
}
/*! 
* \brief Usuń element krajobrazu
*
* Usuwa element krajobrazu o danym id z list 
* \param[in] ide - indeks drona
*/
void Scena::usun_el_kraj(int ide) {
    std::shared_ptr<Interfejs_el_krajobrazu> itmp = Ie[ide];
    std::shared_ptr<Interfejs_rysowania> vtmp = std::reinterpret_pointer_cast<Interfejs_rysowania>(itmp);
    Ir.erase(std::remove(Ir.begin(),Ir.end(),vtmp),Ir.end());
    Ie.erase(std::remove(Ie.begin(),Ie.end(),itmp),Ie.end());
}
/*! 
* \brief Nad elementem
*
* Sprawdzenie warunku, czy dron jest nad płaskozwyżem
* \param[in,out] D - dron, którym operujemy i spawdzamy czy jest nad elementem płaskozwyżem
*/
bool Plaskozwyz::czy_nad(Interfejs_Drona* D) {
    if(sqrt(pow(D->get_srodek2()[0]-get_srodek_e()[0],2)+pow(D->get_srodek2()[1]-get_srodek_e()[1],2))<odleglosc) {
        return true;
    }
    else {
         return false;
    }
}
/*! 
* \brief Nad elementem
*
* Sprawdzenie warunku, czy dron jest nad wzgórzem
* \param[in,out] D - dron, którym operujemy i spawdzamy czy jest nad elementem wzgórzem
*/
bool Wzgorze::czy_nad(Interfejs_Drona* D) {
    if(sqrt(pow(D->get_srodek2()[0]-get_srodek_e()[0],2)+pow(D->get_srodek2()[1]-get_srodek_e()[1],2))<odleglosc) {
        return true;
    }
    else {
         return false;
    }
}
/*! 
* \brief Nad elementem
*
* Sprawdzenie warunku, czy dron jest nad innym dronem
* \param[in,out] D - dron, którym operujemy i spawdzamy czy jest nad  innym dronem
*/
bool Dron::czy_nad(Interfejs_Drona* D) {
    if(sqrt(pow(D->get_srodek2()[0]-get_srodek_e()[0],2)+pow(D->get_srodek2()[1]-get_srodek_e()[1],2))<get_glebokosc1()*2) {
        return true;
    }
    else {
         return false;
    }
}
/*! 
* \brief Nad elementem
*
* Sprawdzenie warunku, czy dron jest nad płaskozwyżem prostym
* \param[in,out] D - dron, którym operujemy i spawdzamy czy jest nad elementem płaskozwyżem prostym
*/
bool Plaskozwyz_prost::czy_nad(Interfejs_Drona* D) {
    if(get_srodek_e()[0]-(get_glebokosc()/2)-D->get_glebokosc1()/2<D->get_srodek2()[0]&&get_srodek_e()[0]+(get_glebokosc()/2)+D->get_glebokosc1()/2>D->get_srodek2()[0]) {
        if(get_srodek_e()[1]-(get_glebokosc()/2)-D->get_glebokosc1()/2<D->get_srodek2()[1]&&get_srodek_e()[1]+(get_glebokosc()/2)+D->get_glebokosc1()/2>D->get_srodek2()[1]) { 
        return true;
    }
    }
         return false;

}
/*! 
* \brief Lądowanie
*
* Sprawdzenie warunku, czy dron może lądować nad płaskozwyżem prostym
* \param[in,out] D - dron, którym operujemy i spawdzamy czy może lądować nad płaskozwyżem prostym
*/
bool Plaskozwyz_prost::czy_ladowac(Interfejs_Drona* D) {
     if(get_srodek_e()[0]-(get_glebokosc()/2)<D->get_srodek2()[0]&&get_srodek_e()[0]+(get_glebokosc()/2)>D->get_srodek2()[0]) {
        if(get_srodek_e()[1]-(get_glebokosc()/2)<D->get_srodek2()[1]&&get_srodek_e()[1]+(get_glebokosc()/2)>D->get_srodek2()[1]) { 
        return true;
    }
    }
        return false;
}

/*! 
* \brief Lądowanie
*
* Funkcja ta sprawia, że dron nie może lądować nad wzgórzem
* \param[in,out] D - dron, którym operujemy i który przelatuje nad dronem
*/
bool Wzgorze::czy_ladowac(Interfejs_Drona* D) {
    return false;
}
/*! 
* \brief Lądowanie
*
* Funkcja ta sprawia, że dron nie może lądować nad innym dronem
* \param[in,out] D - dron, którym operujemy i który przelatuje nad dronem
*/
bool Dron::czy_ladowac(Interfejs_Drona* D) {
    return false;
}

/*! 
* \brief Lądowanie
*
* Sprawdzenie warunku, czy dron może lądować nad płaskozwyżem 
* \param[in,out] D - dron, którym operujemy i spawdzamy czy może lądować nad płaskozwyżem 
*/
bool Plaskozwyz::czy_ladowac(Interfejs_Drona* D) {
   if(sqrt(pow(D->get_srodek2()[0]-get_srodek_e()[0],2)+pow(D->get_srodek2()[1]-get_srodek_e()[1],2))<odleglosc-D->get_glebokosc1()) {
        return true;
    }
    else {
         return false;
    }
}
