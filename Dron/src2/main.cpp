#include <iostream>
#include <iomanip>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "../inc2/Wektor.hh"
#include "../inc2/MacierzRot.hh"
#include "../inc2/Dron.hh"
#include "../inc2/Dr3D_gnuplot_api.hh"



using std::cout;
using std::cin;
using std::endl;


int main() {
  Scena Sc;
  std::vector<Interfejs_rysowania*> elementy;
  Wektor<3> Wek({0,0,0});
  Wektor<3> Wek1({15,5,0});
  Wektor<3> Wek2;
  Wektor<3> Wek3;
  MacierzRot<3> Mac(0,'Z');
  MacierzRot<3> Mac1(90,'Z');
  double _szerokosc=2;
  double _wysokosc=1;
  double _glebokosc=3;
  double wysokosc_pow = 0;
    srand(time(NULL));
   int szerokosc = rand()%7+3;
   int wysokosc=4;
   int glebokosc=rand()%7+3;
    drawNS::Draw3DAPI *rysownik = new drawNS::APIGnuPlot3D(-20,20,-20,20,-20, 20, 0);
    Powierzchnia S(wysokosc_pow, rysownik);
    elementy.push_back(&S);
    Sc.dodaj_el_kraj(rysownik, Wektor<3>({10,-15,0}),MacierzRot<3>(),szerokosc,5,glebokosc);
    elementy.push_back(Sc.get_Ir());
    Sc.dodaj_el_kraj(rysownik, Wektor<3>({-10,10,0}),MacierzRot<3>(),szerokosc,7,glebokosc);
    elementy.push_back(Sc.get_Ir());
    Sc.dodaj_el_kraj(rysownik, Wektor<3>({15,-5,0}),MacierzRot<3>(),szerokosc,7,glebokosc);
    elementy.push_back(Sc.get_Ir());
    Sc.rysuj_wszystkie_elementy(elementy);
    elementy.pop_back();
    elementy.pop_back();
    elementy.pop_back();
    elementy.pop_back();
    Sc.dodaj_drona(rysownik,Wek,Mac,_szerokosc, _wysokosc,_glebokosc);
    elementy.push_back(Sc.get_Ir());
    Sc.dodaj_drona(rysownik,Wek1,Mac1,_szerokosc, _wysokosc,_glebokosc);
    elementy.push_back(Sc.get_Ir());
    Sc.rysuj_wszystkie_elementy(elementy);
    char wybor;
    uint ide=0;
    while (wybor!='k') { 
      cout << "Jest wektorow: " << Wektor<3>::get_jest() << ",a stworzono: " << Wektor<3>::get_stworzono() <<  endl << endl;
    cout << "Powiedz co chcesz zrobić:" << endl
    << "p-zadaj parametry przelotu" << endl
    << "t-pokaz id dronow" << endl
    <<"z-pokaz id elementow(wszystkich)" << endl
    << "m-pokaz menu" << endl
    << "d-dodaj drona" << endl
    << "e-dodaj elemet krajobrazu" << endl
    << "u-usun drona(tylko drona)" << endl
    << "y-usun element krajobrazu(wszystkie elementy)" <<endl
    << "k-koniec dzialania programu" << endl
    << "Twoj wybor: ";
    cin >> wybor;
    switch(wybor) { 
    case 'p':
    double przesuniecie;
    double kat;
    double przesuniecie2;
     if(Sc.get_Id_size()==0) { 
      cout << "Nie można wybrać";
    return 1;}

    else { 
      cout << "Wybierz drona: 0-" << Sc.get_Id_size()-1<< ": ";
      cin >> ide;
    }
    cout<< "Ile ma lecieć w góre: ";
    cin >> przesuniecie;
    cout << "O ile ma się obrócić: ";
    cin >> kat;
    cout << "Ile ma lecieć do przodu: ";
    cin >> przesuniecie2; 
    Sc.get_Id(ide)->animuj(przesuniecie,kat,przesuniecie2, Sc);
    break;
    
    case 't':
    Sc.pokaz_drony();
    break;
    case 'z':
    Sc.pokaz_el_kraj();
    break;
    case 'm':
    cout 
    <<"p-zadaj parametry przelotu" << endl
    << "t-pokaz id dronow" << endl
    <<"z-pokaz id elementow(wszystkich)" << endl
    << "m-pokaz menu" << endl
    << "d-dodaj drona" << endl
    << "e-dodaj elemet krajobrazu" << endl
    << "u-usun drona(tylko drona)" << endl
    << "y-usun element krajobrazu(wszysykie elementy)" <<endl
    << "k-koniec dzialania programu" << endl
    << "Twoj wybor: ";
    break;
    case 'd':
    cout << "Podaj wspolrzedne x,y,z srodka: ";
    cin >> Wek2;
    Sc.dodaj_drona(rysownik,Wek2,Mac,_szerokosc, _wysokosc,_glebokosc);
    elementy.push_back(Sc.get_Ir());
    Sc.rysuj_wszystkie_elementy(elementy);
    break;
    case 'e':
    cout <<"Podaj srodek bryly x,y,z: ";
    cin >> Wek3;
    cout << "Podaj jego wysokosc: ";
    cin >> wysokosc;
    Sc.dodaj_el_kraj(rysownik,Wek3,Mac,szerokosc,wysokosc,glebokosc);
    elementy.push_back(Sc.get_Ir());
    Sc.rysuj_wszystkie_elementy(elementy);
    elementy.pop_back();
    break;
    case 'u':
    if (Sc.get_Id_size()==0) {cout << "Nie ma drona " <<endl; break;}
    cout << "Ktorego drona chcesz usunac 0- " << Sc.get_Id_size()-1 << ": ";
    cin >> ide;
    elementy.erase(elementy.begin()+ide);
    Sc.get_Id(ide)->zmaz_drona();
    Sc.usun_drona(ide);
    Sc.rysuj_wszystkie_elementy(elementy);
    break;
    case 'y':
    if (Sc.get_Ie_size()==0) {cout << "Nie ma dodanych elementow krajobrazu" <<endl; break;}
    cout << "Ktory element chcesz usunac 0-" << Sc.get_Ie_size()-1<< ": " ;
    cin >> ide;
    Sc.get_Ie(ide)->zmaz();
    Sc.rysuj_wszystkie_elementy(elementy);
    Sc.usun_el_kraj(ide);
    break;
    break;
    case 'k':
    delete rysownik;
    break;
    default: 
    cout << "Nie ma takiej opcji. Sprobuj ponownie." << endl;
    break;
    }
    }

}