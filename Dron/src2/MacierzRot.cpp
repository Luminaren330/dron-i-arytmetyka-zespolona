#include "../inc2/MacierzRot.hh"
#include <cmath>

/*! 
* \file
* \brief Definicja metod klasy MacierzRot
*
* Zawiera definicje metod klasy MacierzRot
*/

/*! 
  * \brief Konstruktor
  * 
  * Konstruktor domyślny, który przypisuje wartości jak w macierzy jednostkowej
  * */
template <int Rozmiar>
MacierzRot<Rozmiar>::MacierzRot() {   
for (int i =0; i<Rozmiar; i++) {
    Wektor<Rozmiar> Wek;
    Wek[i]=1;
    wiersze.push_back(Wek);
}
}
/*! 
* \brief Operator przeciążenia *
*
* Mnoży ze sobą  macierze
* \param[in,out] Arg2 - druga macierz, przez która mnożymy
* \return zwraca wynik iloczynu macierzy
*/
template <int Rozmiar>
MacierzRot<Rozmiar> MacierzRot<Rozmiar>::operator * (const MacierzRot<Rozmiar> & Arg2) const { 
    MacierzRot<Rozmiar> wyniki;
    for (int x=0; x<Rozmiar; x++) { 
        for (int y=0; y<Rozmiar; y++) {
            double c=0;
            for ( int k=0; k<Rozmiar; k++) {
                c+=wiersze[x][k] * Arg2[k][y];
            }         
            wyniki.wiersze[x][y]=c;  
        }       
    }
    return wyniki;
    

}
/*! 
* \brief Operator przeciążenia *
*
* Mnoży ze sobą  macierz i wektor
* \param[in,out] Punkt - wektor, przez który mnożymy
* \return zwraca wynik iloczynu macierzy i wektora
*/
template <int Rozmiar>
Wektor<Rozmiar> MacierzRot<Rozmiar>::operator * (const Wektor<Rozmiar> & Punkt) const { 
    Wektor<Rozmiar> wyniki;
    for (int i=0; i<Rozmiar; i++) { 
        for (int j=0; j<Rozmiar; j++) {
            wyniki[i]+=wiersze[i][j] * Punkt[j];
        }       
    }
    return wyniki;


}
  /*! 
  * \brief Konstruktor
  * 
  * Konstruktor, który wykonuje obrót macierzy o dany kąt
  * \param[in] kat_stopnie - obrót macierzy o dany kąt
  * \param[in] os - obrót macierzy o dany oś
  * */
template <int Rozmiar>
MacierzRot<Rozmiar>::MacierzRot(double kat_stopnie, char os) {  
    kat_stopnie=kat_stopnie/180*3.1415926535897932384626433;
        switch(Rozmiar){ 
        case 2:
        wiersze.push_back(Wektor<Rozmiar>({cos(kat_stopnie),-sin(kat_stopnie)}));
        wiersze.push_back(Wektor<Rozmiar>({sin(kat_stopnie),cos(kat_stopnie)}));
        break;
        case 3:
        if (os=='x'|| os=='X') { 
        wiersze.push_back(Wektor<Rozmiar>({1,0,0}));
        wiersze.push_back(Wektor<Rozmiar>({0,cos(kat_stopnie),-sin(kat_stopnie)}));
        wiersze.push_back(Wektor<Rozmiar>({0,sin(kat_stopnie),cos(kat_stopnie)}));
        }
        if (os=='y'|| os=='Y') {
        wiersze.push_back(Wektor<Rozmiar>({cos(kat_stopnie),0,sin(kat_stopnie)}));
        wiersze.push_back(Wektor<Rozmiar>({0,1,0}));
        wiersze.push_back(Wektor<Rozmiar>({-sin(kat_stopnie),0,cos(kat_stopnie)}));
        }
        if (os=='z'|| os=='Z') {
        wiersze.push_back(Wektor<Rozmiar>({cos(kat_stopnie),-sin(kat_stopnie),0}));
        wiersze.push_back(Wektor<Rozmiar>({sin(kat_stopnie),cos(kat_stopnie),0}));
        wiersze.push_back(Wektor<Rozmiar>({0,0,1}));
        }
        break;
        default:
        std::cout << "Nie jest to macierz hiperboliczna" << std::endl;
        break;
        }
    }
/*! 
* \brief Operator przeciążenia []
*
* Sprawdza czy nie odwołaliśmy się poza pamięć i zwraca dany indeks macierzy
* \param[in] ind - zawiera indeks macierzy
* \return zwraca dany indeks macierzy
*/
template <int Rozmiar>
const Wektor<Rozmiar>& MacierzRot<Rozmiar>::operator[] (int ind) const { 
    if (ind < 0|| ind >Rozmiar-1) {
        std::cerr << "Odwolanie poza pamiec" << std::endl;
        exit(0);
    }
    return wiersze[ind];
}
/*! 
* \brief Operator przeciążenia <<
*
* Przeciążenie pozwala pokazać wartości w macierzy
* \param[in, out] Strm - Posiada wartości macierzy
* \param[in,out] Mac - dana macierz
* \return zwraca Strm
*/
template <int Rozmiar>
std::ostream& operator << (std::ostream &Strm, const MacierzRot<Rozmiar> &Mac) { 
for (int i=0; i<Rozmiar; i++) {
    for (int j=0; j<Rozmiar; j++) {
        Strm <<  Mac[i][j] << " ";
    }
    Strm << std::endl;
}    
    return Strm;
}

template class MacierzRot<2>;
template std::ostream& operator << (std::ostream &Strm, const MacierzRot<2> &Mac);
template class MacierzRot<3>;
template std::ostream& operator << (std::ostream &Strm, const MacierzRot<3> &Mac);