#include "../inc2/Wektor.hh"
#include <math.h>

/*! 
* \file
* \brief Definicja metod klasy Wektor
*
* Zawiera definicje metod klasy Wektor
*/

/*! 
* \brief Operator przeciążenia +
*
* Dodaje do siebie wartości współrzędnych wektorów o danym rozmiarze
* \param[in,out] Arg2 - zawiera współrzędne drugiego wektora, do którego dodajemy
* \return zwraca wynik dodawania wektorów
*/
template <int Rozmiar>
 Wektor<Rozmiar> Wektor<Rozmiar>:: operator +(const Wektor<Rozmiar> &Arg2) const { 
    Wektor<Rozmiar> wyniki;
    for (int i=0; i<Rozmiar; i++) {
     wyniki[i]= wsp[i] + Arg2[i];
    }
    return wyniki;
}
/*! 
* \brief Operator przeciążenia -
*
* Odejmuje od siebie wartości współrzędnych wektorów o danym rozmiarze
* \param[in,out] Arg2 - zawiera współrzędne drugiego wektora, od którego odejmujemy
* \return zwraca wynik odejmowania wektorów
*/
template <int Rozmiar>
Wektor<Rozmiar> Wektor<Rozmiar>::operator -(const Wektor<Rozmiar> &Arg2) const { 
     Wektor<Rozmiar> wyniki;
    for (int i=0; i<Rozmiar; i++) {
     wyniki[i]= wsp[i] - Arg2[i];
    }
    return wyniki;
}
/*! 
* \brief Operator przeciążenia *
*
* Mnoży wektor o danym rozmiarze z liczbą 
* \param[in,out] Arg2 - dana liczba, przez która mnożymy wektor
* \return zwraca wynik mnożenia
*/
template <int Rozmiar>
 Wektor<Rozmiar> Wektor<Rozmiar>::operator *(double &Arg2) const {  
    Wektor<Rozmiar> wyniki;
    for (int i=0; i<Rozmiar; i++) {
     wyniki[i]= wsp[i] * Arg2;
    }
    return wyniki;
}
/*! 
* \brief Operator przeciążenia *
*
* Mnoży ze sobą wartości współrzędnych wektorów o danym rozmiarze (iloczyn skalarny)
* \param[in,out] Arg2 - zawiera współrzędne drugiego wektora, z którym mnożymy
* \return zwraca wynik iloczynu skalarnego
*/
template <int Rozmiar>
double Wektor<Rozmiar>::operator *(const Wektor<Rozmiar> &Arg2) const { 
    double wynik=0;
    for (int i=0; i<Rozmiar; i++) {
        wynik = wynik + (wsp[i] * Arg2[i]);
    }
    return wynik;
}
/*! 
* \brief Długość wektora
*
* Liczy długość wektora o danym rozmiarze i zwraca go
* \return zwraca długość wektora
*/
template <int Rozmiar>
double Wektor<Rozmiar>::dlugosc() const { // Dlugosc wektora
    double dlugosc;
    if (Rozmiar==2) { 
    dlugosc=sqrt(pow(wsp[0],2)+ pow(wsp[1],2));
    return dlugosc;
    }
    if (Rozmiar==3) {
    dlugosc=sqrt(pow(wsp[0],2)+pow(wsp[1],2)+pow(wsp[2],2));
    return dlugosc;
    }  
    else {
        return 0; 
    }  
}
/*! 
* \brief Operator przeciążenia []
*
* Sprawdza czy nie odwołaliśmy się poza pamięć i zwraca dany indeks wektora
* \param[in] ind - zawiera indeks wektora
* \return zwraca dany indeks wektora
*/
template <int Rozmiar>
double & Wektor<Rozmiar>::operator[] (int ind) { 
       if (ind < 0|| ind >Rozmiar-1) {
        std::cerr << "Odwolanie poza pamiec" << std::endl;
        exit(0);
    }
    return wsp[ind];
}
/*! 
* \brief Operator przeciążenia []
*
* Sprawdza czy nie odwołaliśmy się poza pamięć i zwraca dany indeks wektora
* \param[in] ind - zawiera indeks wektora
* \return zwraca dany indeks wektora
*/
template <int Rozmiar>
const double & Wektor<Rozmiar>::operator[]  (int ind) const { 
    if (ind < 0|| ind >Rozmiar-1) {
        std::cerr << "Odwolanie poza pamiec" << std::endl;
        exit(0);
    }
    return wsp[ind];
}
/*! 
* \brief Operator przeciążenia >>
*
* Przeciążenie pozwala przypisać wartości do wektora
* \param[in, out] Strm - zawiera wartość przypisywaną
* \param[in,out] Wek - przypisanie wartości nadanej do wektora
* \return zwraca Strm
*/
template <int Rozmiar>
std::istream& operator >> (std::istream &Strm, Wektor<Rozmiar> &Wek) { // Przeciążenie operatora >>
    double d;
    for (int i=0; i<Rozmiar; i++) { 
    Strm >> d; Wek[i]=d;
    }   
    return Strm;

}
/*! 
* \brief Operator przeciążenia <<
*
* Przeciążenie pozwala pokazać wartości w wektorze
* \param[in, out] Strm - Posiada wartości wektora
* \param[in,out] Wek - dany wektor
* \return zwraca Strm
*/
template <int Rozmiar>
std::ostream& operator << (std::ostream &Strm, const Wektor<Rozmiar> &Wek) { // Przeciążenie operatora <<
    for (int i=0; i<Rozmiar; i++) { 
    Strm << Wek[i] << " ";
    }
    Strm << std::endl;
    return Strm;

    
}

template class Wektor<2>;
template class Wektor<3>;
template class Wektor<6>;
template std::istream& operator >> (std::istream &Strm, Wektor<2> &Wek);
template std::istream& operator >> (std::istream &Strm, Wektor<3> &Wek);
template std::istream& operator >> (std::istream &Strm, Wektor<6> &Wek);
template std::ostream& operator << (std::ostream &Strm, const Wektor<2> &Wek);
template std::ostream& operator << (std::ostream &Strm, const Wektor<3> &Wek);
template std::ostream& operator << (std::ostream &Strm, const Wektor<6> &Wek);