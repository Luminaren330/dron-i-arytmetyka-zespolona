#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH

#include <iostream>
using std::cout;
using std::cin;
using std::endl;
/*!
 *  Plik zawiera definicje struktury LZesplona oraz zapowiedzi
 *  przeciazen operatorow arytmetycznych dzialajacych na tej
 *  strukturze.
 */


/*!
 * Modeluje pojecie liczby zespolonej
 */
class LZespolona {
    private:
    double   re;    /*! Pole repezentuje czesc rzeczywista. */
    double   im;    /*! Pole repezentuje czesc urojona. */
    public:
    LZespolona  operator + (const LZespolona &L2) const;
    LZespolona  operator - (const LZespolona &L2) const;
    LZespolona  operator * (const LZespolona &L2) const;
    LZespolona  operator / (const LZespolona &L2) const;
    LZespolona  operator / (const double &L2) const;  
    double Modul2() const;
    LZespolona Sprzezenie() const;
    bool operator == (const LZespolona &Skl2) const; 
    bool operator != (const LZespolona &Skl2) const;
    LZespolona (double _re, double _im): re(_re), im(_im){} /*! Wywołanie konstruktorów, inicjalizacja */
    LZespolona() {} /*! Dopisanie konstruktora */  
    double get_re() const {return re;}
    double get_im() const {return im;}
    void set_re(double _re) {re=_re;}
    void set_im(double _im) {im=_im;}
};
    std::ostream& operator << (std::ostream& out,  const LZespolona &lzes);
    std::istream& operator >> (std::istream& in, LZespolona &lz);

// std::ostream& operator << (std::ostream& out,  const LZespolona &lzes);
// std::istream& operator >> (std::istream& in, LZespolona &lz);



#endif
