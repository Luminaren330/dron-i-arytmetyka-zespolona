#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH



#include "../inc/LZespolona.hh"

/*!
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator { Op_Dodaj, Op_Odejmij, Op_Mnoz, Op_Dziel };


/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego
 */
class WyrazenieZesp {
    private:
    LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego
    Operator     Op;     // Opertor wyrazenia arytmetycznego
    LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego
    public:
    LZespolona Oblicz() const;
    WyrazenieZesp (LZespolona _Arg1, Operator _Op, LZespolona _Arg2): Arg1(_Arg1), Op(_Op), Arg2(_Arg2){}
    WyrazenieZesp () {}
    LZespolona get_Arg1() const {return Arg1;};
    Operator get_Op() const {return Op;};
    LZespolona get_Arg2() const {return Arg2;};
    void set_Arg1(LZespolona _Arg1) {Arg1 = _Arg1;};
    void set_Op(Operator _Op) {Op = _Op;};
    void set_Arg2(LZespolona _Arg2) {Arg2 = _Arg2;};
};



std::ostream& operator << (std::ostream& out, const WyrazenieZesp &wz);
std::ostream& operator << (std::ostream& out, const Operator &O);
std::istream& operator >> (std::istream& in, WyrazenieZesp &wz);
std::istream& operator >> (std::istream& in, Operator &O);

#endif