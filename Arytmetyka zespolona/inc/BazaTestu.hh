#ifndef BAZATESTU_HH
#define BAZATESTU_HH


#include "../inc/WyrazenieZesp.hh"
#include <fstream>
#include <string.h>

/*
 * Modeluje pojecie baze testu z zestawem pytan w tablicy
 * oraz informacji o maksymalnej ilosci pytan, jak
 * tez indeksem nastepnego pytania, ktore ma byc pobrane
 * z bazy.
 */
class BazaTestu {
    private:
    std::fstream strm_plik_pytan;
    public:
    bool koniec_pytan (WyrazenieZesp &W);
    bool pobierz_pytanie (WyrazenieZesp &W);
    bool otworz_plik (std::string nazwa);
    int inicjalizacja(const char  *sNazwaTestu);
    void wyszysc();  
};



#endif
