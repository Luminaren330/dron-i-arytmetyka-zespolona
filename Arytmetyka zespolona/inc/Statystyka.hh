
#ifndef STATYSTYKA_HH
#define STATYSTYKA_HH

#include "../inc/LZespolona.hh"
#include "../inc/BazaTestu.hh"

class Statystyki {
    private:
    int lpoprawnych=0;
    int lpytan=0;
    public:
    void dodaj_poprawna();
    void dodaj_niepoprawna();
    void Wypisz_Wynik();
    Statystyki (int _lpoprawnych, int _lpytan): lpoprawnych(_lpoprawnych), lpytan(_lpytan) {}
    Statystyki () {}
};




#endif //STATYSTYKA_HH
