#include "../inc/WyrazenieZesp.hh"


LZespolona WyrazenieZesp::Oblicz() const { /*! Funkcja licząca operacje na liczbach zespolonych */
    switch(Op) {
        case Op_Dodaj:
            return Arg1 + Arg2;
            break;
        case Op_Odejmij:
            return Arg1 - Arg2;
            break;
        case Op_Mnoz:
            return Arg1 * Arg2;
            break;
        case Op_Dziel:
            return Arg1 / Arg2;
            break;
    }
    return LZespolona(0,0); /*! Zwraca zero gdy żadna operacja nie jest wyżej z podanych */

}
std::ostream& operator << (std::ostream& out, const WyrazenieZesp &wz) { /*! Przęciążenie operatora wyświetlania wyrażenia zespolonego */
    out << wz.get_Arg1() << wz.get_Op() << wz.get_Arg2();
    return out;

}
std::ostream& operator << (std::ostream& out, const Operator &O) { /*! Przęciążenie operatora, sprawdzanie która operacja jest wykonywana */
    switch (O) {
        case Op_Dodaj: out << " + "; break;
        case Op_Odejmij: out << " - "; break;
        case Op_Mnoz: out << " * "; break;
        case Op_Dziel: out << " / "; break;
    }
    return out;
}
std::istream& operator >> (std::istream& in, WyrazenieZesp &wz) { /*! Przęciążenie operatora czytania wyrażenia zespolonego */
    LZespolona lz;
    Operator o;
    in >> lz; wz.set_Arg1(lz);
    in >> o; wz.set_Op(o);
    in >> lz; wz.set_Arg2(lz);
    return in;
}
std::istream& operator >> (std::istream& in, Operator &O) { /*! Przęciążenie operatora, sprawdzanie która operacja jest wykonywana */
    char znak;
    in >> znak;
    switch(znak) {
        case '+':O=Op_Dodaj; break;
        case '-':O=Op_Odejmij; break;
        case '*':O=Op_Mnoz; break;
        case '/':O=Op_Dziel; break;
        default: in.setstate(std::ios::failbit);
    }
    return in;
}