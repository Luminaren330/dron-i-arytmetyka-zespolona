#include <iostream>
#include "../inc/BazaTestu.hh"
#include "../inc/Statystyka.hh"
#include <limits>

using std::cout;
using std::cin;
using std::endl;


int main(int argc, char **argv)
{
  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }


  BazaTestu   BazaT;
 

WyrazenieZesp   WyrZ_PytanieTestowe;   
    LZespolona lz;
    Statystyki licznik;
    
 
  
  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;
  if (BazaT.inicjalizacja(argv[1])==1) { /*! Inicjlizuje odpowiedni test*/
  BazaT.otworz_plik("latwe.dat");
  }
  else if (BazaT.inicjalizacja(argv[1])==2) {
    BazaT.otworz_plik("trudne.dat");
  }
  else if (BazaT.inicjalizacja(argv[1])==0) {
    std::cerr << " Inicjalizacja testu nie powiodla sie." << endl;
     return 1;
  }
    while (BazaT.koniec_pytan(WyrZ_PytanieTestowe)) { 
      if (BazaT.pobierz_pytanie(WyrZ_PytanieTestowe)) {  /*! jeśli zwraca true to wykonuje test dla tej linijki z pliku*/
      bool condition =false; /*! Sprawdza czy po raz 3 nadal nie wpisaliśmy dobrze liczby */  
        for (int j=0;j<3;j++) {
            cout << "Podaj wynik operacji: " << WyrZ_PytanieTestowe << endl;   /*! Pętla służąca głownie do sprawdzenia warunku, czy dobrze wpisana została
            liczba zespolona */
            cout << "Twoja odpowiedz: ";
            cin >> lz;
            if (cin.fail()) {
                cout << "Blad zapisu liczby zespolonej." << endl;
                cin.clear();
                cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n'); /*! Wyczyszcza to co wpisało się w lz */
            }
            else  {
              condition=true;
              if (lz==WyrZ_PytanieTestowe.Oblicz()) {
                cout << "Odpowiedz poprawna" << endl;
                licznik.dodaj_poprawna();
                break;
              }
              else {
                cout << "Bledna odpowiedz. Poprawnym wynikiem jest:" << WyrZ_PytanieTestowe.Oblicz() << endl;
                licznik.dodaj_niepoprawna();
                break;
              }             
                
            }
        }  
        if (condition==false) {
          cout << "Koniec prob" << endl;
          licznik.dodaj_niepoprawna();
        }                     
    }
    else {
      BazaT.wyszysc(); /*! Jeśli zła linijka to pomija ją*/
    }
    }
    
    licznik.Wypisz_Wynik();
    cout << endl;

  }

  /*cout << endl;
  cout << " Koniec testu" << endl;
  cout << endl;*/


