
#include "../inc/Statystyka.hh"
#include <iomanip>


void Statystyki::dodaj_poprawna()  { /*! Funkcja dodająca odpowiedz poprawna */
    lpytan++;
    lpoprawnych++;   
}

void Statystyki::dodaj_niepoprawna() {
    lpytan++;
}

void Statystyki::Wypisz_Wynik() { /*! Funkcja wypisująca wynik statystyk */
    cout << endl;
    cout <<  "Koniec testu" << endl;
    cout << "Ilosc dobrych odpowiedzi: " << lpoprawnych << endl;
    int bledne;
    bledne=lpytan-lpoprawnych;
    cout << "Ilosc zlych odpowiedzi: " << bledne << endl;
    double wynikproc;
    wynikproc =((double)lpoprawnych/(double)lpytan) *100;
    cout << std::fixed<< std::setprecision(1) << "Wynik procentowy poprawnych odpowiedzi: " << wynikproc << " %" << endl;
}