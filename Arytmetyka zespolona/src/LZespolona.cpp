#include "../inc/LZespolona.hh"



/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */

LZespolona LZespolona::Sprzezenie() const {  /*! Sprzęża argument urojony liczby*/
     return LZespolona(re, -im);   
}
double LZespolona::Modul2() const {    /*! Tworzy moduł z liczby zespolonej */
    return (re * re + im * im);
}
 LZespolona LZespolona::operator + (const LZespolona &Skl2) const /*! Przęciążenie operatora dodawania */
{
    LZespolona  Wynik;

    Wynik.re = re + Skl2.re;
    Wynik.im = im + Skl2.im;
    return Wynik;

}
LZespolona LZespolona::operator - (const LZespolona &Skl2) const /*! Przęciążenie operatora odejmowania */
{
    LZespolona  Wynik;

    Wynik.re = re - Skl2.re;
    Wynik.im = im - Skl2.im;
    return Wynik;
}
LZespolona LZespolona::operator * (const LZespolona &Skl2) const /*! Przęciążenie operatora mnożenia */
{
    LZespolona  Wynik;

    Wynik.re = (re * Skl2.re) + (-(im * Skl2.im));
    Wynik.im = (im * Skl2.re) + (re * Skl2.im);
    return Wynik;
}
LZespolona LZespolona::operator / (const LZespolona &Skl2) const /*! Przęciążenie operatora dzielenia */
{

    return ((*this) * Skl2.Sprzezenie())/Skl2.Modul2();

}
LZespolona LZespolona::operator / (const double &Skl2) const { /*! Przęciążenie operatora dzielenia */

    LZespolona  Wynik;

    Wynik.re = re / Skl2;
    Wynik.im = im / Skl2;
    return Wynik;

}
std::ostream& operator << (std::ostream& out,  const LZespolona &lzes) { /*! Przęciążenie operatora wyświetlania liczby zespolonej */

    out << "(" << lzes.get_re() << std::showpos <<  lzes.get_im() << std::noshowpos << "i)";
    return out;
}
std::istream& operator >> (std::istream& in, LZespolona &lz) { /*! Przęciążenie operatora czytania liczby zespolonej */
    char c;
    double d;
    in >> c;
    if(c!='(') {
        in.setstate(std::ios::failbit); /*! Zabezpieczenie przed wprowadzeniem niepożądanego znaku */
    } else {
        in >> d; lz.set_re(d);
        in >> c;
        if(c!='+'&& c!='-') {
            in.setstate(std::ios::failbit);
        }
        else {
            in >> d; lz.set_im(d);
            if(c=='-') lz.set_im(-lz.get_im());
            in >> c;
            if (c != 'i') {
                in.setstate(std::ios::failbit);
            } else {
                in >> c;
                if (c != ')') {
                    in.setstate(std::ios::failbit);
                }
            }
        }
    }
    return in;

}
bool LZespolona::operator == (const LZespolona &Skl2) const { /*! Przęciążenie operatora porównania liczby zespolonej i wyniku */
    const double epsilon = 0.00000001;
    if (std::abs(re-Skl2.re)>epsilon || std::abs(im-Skl2.im)>epsilon) {
        return false;
    }
    else  return true;

}
bool LZespolona::operator != (const LZespolona &Skl2) const {
    return !(LZespolona()==Skl2);
}