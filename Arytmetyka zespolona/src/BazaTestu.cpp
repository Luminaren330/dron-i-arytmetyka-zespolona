#include <iostream>
#include <cstring>
#include <cassert>
#include "../inc/BazaTestu.hh"
#include <limits>

using namespace std;

using std::cout;
using std::cin;
using std::endl;


bool BazaTestu::otworz_plik(std::string nazwa) { /*! otwiera plik, ktory chcemy*/
    strm_plik_pytan.open(nazwa, std::fstream::in);
    if (strm_plik_pytan.good()) {
        return true;
    }
    else {
        return false;
    }
}
bool BazaTestu::pobierz_pytanie (WyrazenieZesp &W) {    /*! Sprawdza czy dobrze zostało wprowadzone wyrażenie zespolone w pliku*/
    if (strm_plik_pytan.good()) {
        return true;
    }
    else {
        return false;
    }
}
bool BazaTestu::koniec_pytan (WyrazenieZesp &W) { /*! Wczytuje z pliku jako Wyrażenie zespolone i sprawdza czy koniec pliku*/
    strm_plik_pytan >> W;
    if (!strm_plik_pytan.eof()) {
        return true;
    }
    else {
        return false;
    }
}
void BazaTestu::wyszysc() { /*! pomija linijkę jeśli jest błędna */
    strm_plik_pytan.clear();
    strm_plik_pytan.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
}
int BazaTestu::inicjalizacja(const char  *sNazwaTestu) { /*! Inicjalizuje test, który użytkownik chce wywołać*/
    if (!strcmp(sNazwaTestu,"latwy")) {
        return 1;
    }
    else if (!strcmp(sNazwaTestu,"trudny")) {
        return 2;
    }   
    return 0;
}





